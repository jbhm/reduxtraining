import { StyleSheet, View, StatusBar, AppRegistry } from 'react-native';
import React, { Component } from 'react'
import { Provider } from 'react-redux';
import PlayerContainerComponent from './app/containerComponents/Player/PlayerContainerComponent';
import Store from './app/logic/utils/Store';

class App extends React.PureComponent {
  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>          
          <PlayerContainerComponent />
        </View>
      </Provider>
    );
  }
}

const store = Store.getStore()

AppRegistry.registerComponent('ReduxTutorial', () => App);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
})
