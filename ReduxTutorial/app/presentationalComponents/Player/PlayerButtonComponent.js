import React from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';


const PLAYER_BUTTON_INTERACTIONS_ENUM = Object.freeze({
    ORDER_BUTTON_PRESSED : "ORDER_BUTTON_PRESSED",
});


export default class PlayerButtonComponent extends React.Component {

    static get PLAYER_BUTTON_INTERACTIONS() {
        return PLAYER_BUTTON_INTERACTIONS_ENUM;
    }

    render() {
        return (
            <View style={styles.headerContainer}>
                <Button
                    onPress={() => { this.pressOrderButton() }}
                    title='Order Button'
                    color='blue'
                />
            </View>
        )
    }

    pressOrderButton = () =>  {

        this.props.interactionHandler(PLAYER_BUTTON_INTERACTIONS_ENUM.ORDER_BUTTON_PRESSED);
    }
}


const styles = StyleSheet.create({
    headerContainer: {
        height: 30,
        flexDirection: 'row',
        marginTop: 20,
        justifyContent: 'space-around'
    },
})
