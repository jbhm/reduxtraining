import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default class PlayerHeaderComponent extends React.Component {

    render() {
        return (
            <View style={styles.headerContainer}>
                <Text style={styles.headerText}>
                    Friday Top Movers
            </Text>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    headerContainer: {
        display: 'flex',
        marginTop: 30,
        alignItems: 'center',
    },
    headerText: {
        fontWeight: 'bold',
        fontSize: 22,
    }
})
