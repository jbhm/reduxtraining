import React from 'react';
import { View, Text, StyleSheet, ActivityIndicator, Dimensions, FlatList } from 'react-native';
import * as selectors from '../../logic/selectors';
import { connect } from 'react-redux';
import { ActionCreators } from '../../logic/actions';
import { bindActionCreators } from 'redux';

const screenWidth = Dimensions.get('window').width;


class PlayerListComponent extends React.Component {

    render() {
        return (
            <View style={styles.container}>
                <View>
                    {this.props.playerData.isPending ?
                        <View style={styles.activityIndicator}>
                            <ActivityIndicator
                                size="large"
                            />
                        </View>
                        :
                        <View>
                            <FlatList
                                data={this.props.filteredOrderedPlayerData}
                                renderItem={this.renderItem}
                                inverted={false}
                                ref={(FlatList) => { this.flatList = FlatList; }}
                                keyExtractor={(item, index) => index.toString()}
                            >
                            </FlatList>
                        </View>
                    }
                </View>
            </View>
        )
    }

    renderItem = ({ item }) => {

        let positive = item.yield > 0 ? true : false;

        return (
            <View style={styles.listCell}>
                <Text style={styles.listTitleText}> {item.name} </Text>
                <Text style={positive ? styles.listValuePositiveText : styles.listValueNegativeText}> {item.yield} </Text>
            </View>
        );
    }
}



const makeMapStateToProps = () => {

    const getFilteredOrderedPlayers = selectors.creators.makeGetSortedFilteredPlayers();

    const mapStateToProps = (state, props) => {
        return {
            filteredOrderedPlayerData: getFilteredOrderedPlayers(state, props),
            playerData: state.playerData
        }
    }
    return mapStateToProps
}

function mapDispatchToProps(dispatch) {
    return Object.assign({ dispatch: dispatch }, bindActionCreators(ActionCreators, dispatch));
}

export default connect(makeMapStateToProps, mapDispatchToProps)(PlayerListComponent);




const styles = StyleSheet.create({
    container: {
        marginTop: 30,
        height: 600
    },
    listCell: {
        height: 30,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    listTitleText: {
        color: 'black',
        textAlign: 'left',
    },
    listValuePositiveText: {
        color: 'green',
        textAlign: 'right',
    },
    listValueNegativeText: {
        color: 'red',
        textAlign: 'right',
    },
    activityIndicator: {
        marginTop: 200,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'stretch',
        width: screenWidth,
    }
})

