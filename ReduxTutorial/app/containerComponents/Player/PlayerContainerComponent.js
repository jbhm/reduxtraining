import React from 'react';
import { connect } from 'react-redux';
import { ActionCreators } from '../../logic/actions';
import { bindActionCreators } from 'redux';

import { View, } from 'react-native';
import PlayerHeaderComponent from '../../presentationalComponents/Player/PlayerHeaderComponent';
import PlayerListComponent from '../../presentationalComponents/Player/PlayerListComponent';
import PlayerButtonComponent from '../../presentationalComponents/Player/PlayerButtonComponent';

console.disableYellowBox = true;



class PlayerContainerComponent extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            orderButtonPressed: false,
        }
    }


    componentWillMount() {

        this.props.getPlayerData().then(() => {
        });
    }


    interactionHandler = (type, value) => {

        switch (type) {
            case PlayerButtonComponent.PLAYER_BUTTON_INTERACTIONS.ORDER_BUTTON_PRESSED:

                let newOrderButtonState = !this.state.orderButtonPressed;

                this.setState({ orderButtonPressed: newOrderButtonState })

                break;
            default:
                break;
        }
    }

    render() {
        return (
            <View>
                <PlayerHeaderComponent />
                <PlayerButtonComponent
                    interactionHandler={this.interactionHandler}
                />
                <PlayerListComponent
                    playerData={this.props.playerData}
                    orderButtonPressed={this.state.orderButtonPressed}
                />
            </View>
        );
    }
}


const makeMapStateToProps = () => {
    const mapStateToProps = (state, props) => {
        return {

            playerData: state.playerData
        }
    }
    return mapStateToProps
}

function mapDispatchToProps(dispatch) {
    return Object.assign({ dispatch: dispatch }, bindActionCreators(ActionCreators, dispatch));
}

export default connect(makeMapStateToProps, mapDispatchToProps)(PlayerContainerComponent);
