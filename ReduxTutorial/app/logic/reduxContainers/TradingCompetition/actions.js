import * as types from './types';
import HttpUtils from '../../utils/HttpUtils';
import Player from '../../entities/TradingCompetition/Player';
import Store from '../../utils/Store'



export function getPlayerData() {

    return dispatch => {
        return new Promise((resolve, reject) => {
            dispatch({ type: types.PLAYER_DATA_REQUEST_PENDING });

            requestPlayerData(
                function (resp, error) {
                    if (error == null) {

                        let processedData = processPlayerData(resp);

                        dispatch({
                            type: types.PLAYER_DATA_REQUEST_SUCCESS,
                            response: resp,
                            data: processedData
                        })

                        resolve(processedData);

                    } else {
                        dispatch({
                            type: types.PLAYER_DATA_REQUEST_ERROR,
                            error
                        });
                        reject(error);
                    }
                }
            );
        });
    }
}



var processPlayerData = (rawPlayerData) => {

    let players = new Array();

    for (let d of rawPlayerData) {

        let player = new Player();

        player.name = d.name;
        player.yield = new Number(d.yield).valueOf();

        players.push(player);
    }

    return players;
}


var requestPlayerData = (callBack) => {

    let param = "v2/5b8322803300004d00959506";

    HttpUtils.get(param).then(resp => {
        callBack(resp, null);
    }).catch((error) => {
        callBack(null, error);
    });
}