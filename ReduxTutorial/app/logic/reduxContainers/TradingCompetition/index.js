import * as TradingCompetitionActions from './actions';
import * as TradingCompetitionReducer from './reducer';
import * as TradingCompetitionTypes from './types';

export const Client = Object.assign({},
    TradingCompetitionActions, 
    TradingCompetitionReducer,
    TradingCompetitionTypes,
);