import { createSelector } from 'reselect';
import Player from '../../entities/TradingCompetition/Player';

var _ = require('lodash');


//region input selectors

const getPlayers = (state, props) => state.playerData.data;
const getOrderState = (state, props) => props.orderButtonPressed;



//region selector creators

export const makeGetSortedFilteredPlayers = () => {
    return createSelector(
        [getPlayers, getOrderState],
        (players, order) => {

            let filteredSortedPlayers = _.cloneDeep(players);

            if (order) {

                if (filteredSortedPlayers != undefined && filteredSortedPlayers.constructor === Array) {

                    filteredSortedPlayers = _.orderBy(filteredSortedPlayers, 'name', 'asc');
                }

            }

            console.log("test");

            return filteredSortedPlayers;
        }
    )
}