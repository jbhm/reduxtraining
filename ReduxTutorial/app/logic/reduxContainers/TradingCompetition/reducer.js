import createReducer from '../../utils/createReducer';
import * as types from './types';


const INITIAL_STATE = {
    isPending: false, responseDate: undefined, error: undefined, response: {}, data: {}
  }

export const playerData = createReducer(INITIAL_STATE, {
    [types.PLAYER_DATA_REQUEST_PENDING](state, action){
        return Object.assign({}, state, {
            error: undefined,
            isPending: true,
            responseDate: undefined
        });
    },
    [types.PLAYER_DATA_REQUEST_SUCCESS](state, action) {
        return Object.assign({}, state, {
            response: action.response,
            data: action.data,
            error: undefined,
            isPending: false,
            responseDate: Date.now()
        });
    },
    [types.PLAYER_DATA_REQUEST_ERROR](state, action) {
        let newState = action.error
        return Object.assign({}, state, {
            error: newState,
            isPending: false,
            responseDate: undefined
        });
    }
});