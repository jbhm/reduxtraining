import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import reducer from '../reducers';

var Store = (function () {
    var instance;
    const loggerMiddleware = createLogger({ predicate: (getState, action) => __DEV__ });

    function configureStore(initialState) {
        const enhancer = compose(
            applyMiddleware(
                thunkMiddleware,
                loggerMiddleware,
            ),
        );
        return createStore(reducer, initialState, enhancer);
    }

    return {
        getStore: function () {
            if (!instance) {
                instance = configureStore({});
            }
            return instance
        }
    }

})();
module.exports = Store;
