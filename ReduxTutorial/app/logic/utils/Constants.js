import React, { Component } from 'react';


//request urls

// const HOST_URL = "http://www.mocky.io/v2/5b8302e433000053009594fb";
const HOST_URL = "http://www.mocky.io/";

export default class Constants extends Component {

    static get HOST_URL() {
        return HOST_URL;
    }
}