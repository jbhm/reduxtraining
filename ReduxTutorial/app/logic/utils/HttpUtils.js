import Constants from './Constants';

class HttpUtils {

  static REQUEST_TYPES = Object.freeze({
    JSON: "JSON",
    XML: "XML",
  });

  static jsonHeaders() {
    return {
      'Content-Type': 'application/json',
    }
  }

  static xmlHeaders() {
    return {
      'Content-Type': 'text/xml',
    }
  }


  static get(route, host = Constants.HOST_URL, type = HttpUtils.REQUEST_TYPES.JSON) {

    return type == HttpUtils.REQUEST_TYPES.JSON ? this.jsonRequest(host, route, null, 'GET') : this.xmlRequest(host, route, null, 'GET');
  }

  static put(route, params, host = Constants.HOST_URL, type = Constants.REQUEST_TYPES.JSON) {

    return type == HttpUtils.REQUEST_TYPES.JSON ? this.jsonRequest(host, route, params, 'PUT') : this.xmlRequest(host, route, params, 'PUT');
  }

  static post(route, params, host = Constants.HOST_URL, type = Constants.REQUEST_TYPES.JSON) {

    return type == HttpUtils.REQUEST_TYPES.JSON ? this.jsonRequest(host, route, params, 'POST') : this.xmlRequest(host, route, params, 'POST');
  }

  static delete(route, params, host = Constants.HOST_URL, type = Constants.REQUEST_TYPES.JSON) {

    return type == HttpUtils.REQUEST_TYPES.JSON ? this.jsonRequest(host, route, params, 'DELETE') : this.xmlRequest(host, route, params, 'DELETE');
  }

  
  static jsonRequest(newHost, route, params, verb) {

    const host = newHost
    const url = `${host}${route}`
    let options = Object.assign({ method: verb }, params ? { body: JSON.stringify(params) } : null);
    options.headers = HttpUtils.jsonHeaders()

    console.log('HttpUtils Url: ' + url);

    return fetch(url, options).then(resp => {
      console.log('HttpUtils response: ' + resp);
      let json = resp.json();
      if (resp.ok) {
        return json
      }
      return json.then(err => { throw err });
    })
  }

  //TODO
  static xmlRequest(newHost, route, params, verb) {

    const host = newHost
    const url = `${host}${route}`
    let options = Object.assign({ method: verb }, params ? { body: JSON.stringify(params) } : null);
    options.headers = HttpUtils.xmlHeaders()

    console.log('HttpUtils Url: ' + url);

    return fetch(url, options).then(resp => {
      console.log('HttpUtils response: ' + resp);

      let respText = resp.text();

      if (resp.ok) {

        return respText
      }
      return respText.then(err => { throw err });
    })
  }
}

export default HttpUtils
