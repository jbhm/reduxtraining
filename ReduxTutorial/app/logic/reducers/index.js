import { combineReducers } from 'redux';
import * as TradingCompetitionReducer from '../reduxContainers/TradingCompetition/reducer';

export default combineReducers(Object.assign({},
    TradingCompetitionReducer,
));