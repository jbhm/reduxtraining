import * as TradingCompetitionSelectors from '../reduxContainers/TradingCompetition/selectors';

export const creators = Object.assign({},
    TradingCompetitionSelectors,
);